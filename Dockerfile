FROM node:latest
USER root
COPY . .
EXPOSE 80
CMD ["node", "test.js"]
